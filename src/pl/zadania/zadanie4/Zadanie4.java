package pl.zadania.zadanie4;

import java.util.Scanner;

/**
 * creates array with size specified by user
 * moves right integers in the table and prints their values
 * @author Joker
 */

public class Zadanie4 {

    public static void main(String [] args) {

        int intArraySize = 0;

        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Ile liczb ma mieć Twoja tablica?");
        intArraySize = inputScanner.nextInt();
        int [] intArray = new int [intArraySize]; //new array, size specified by user

        for (int i = 0; i<intArraySize; i++){
            System.out.println("Podaj " +(i + 1) + ". liczbę:");
            intArray[i] = inputScanner.nextInt(); //user specifies numbers in an array
        }

        int intIndex = intArraySize -1; 
        int intLast = intArray[intIndex]; //last integer in the array (highest index)

	System.out.println("Liczby z Twojej tablicy to:");
        	for (int i = intIndex; i>0; i--){
            		intArray[i] = intArray[i-1];
           	 	System.out.print(intArray[i] + " ");
        	}
        System.out.println(intLast);

    }
}
