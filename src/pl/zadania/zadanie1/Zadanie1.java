package pl.zadania.zadanie1; 
import java.util.Scanner;

/**
 *shows user all number between 2 numbers chosen
 *@author Joker
 */

public class Zadanie1 {

	public static void main(String[] args) {

		int intFirst;
		int intSecond;
		int i;
		
		Scanner inputScanner = new Scanner(System.in);
		System.out.println("Enter the first integer:");
		intFirst = inputScanner.nextInt();
		//System.out.println(intFirst);
		System.out.println("Enter the second integer:");
		intSecond = inputScanner.nextInt();
		//System.out.println(intSecond);

       		if (intFirst < intSecond) {
			i = intFirst+1;
			System.out.println("Between your integers are:");
			while (i < intSecond) {
				System.out.print((i++) + " ");
			}
		} else if (intFirst > intSecond) {
			i = intFirst-1;
			System.out.println("Between your integers are:");
			while (i > intSecond) {
				System.out.print((i--) + " ");
			}
		} else {
			System.out.print("Numbers are equal.");
		}
		System.out.println("");		
	}
}
