package pl.zadania.zadanie3;

import java.util.Scanner;

/**
 *calculates sum and product of table entered by user
 *@author Joker
 */

public class Zadanie3 {

	public static void main(String [] args) {

	int intArraySize = 0;
	int intSum = 0;
	int intProduct = 1;	

	Scanner inputScanner = new Scanner(System.in);
	System.out.println("Ile liczb ma mieć Twoja tabela?");
	intArraySize = inputScanner.nextInt();
	int [] intArray = new int [intArraySize]; //new array with size specified by user

	if (intArraySize <= 0) {
		System.out.println("Wprowadź liczbę większą od 0");
	} else {
		for (int i = 0; i < intArraySize; i++){
			System.out.println("Podaj " + (i + 1) + ". liczbę:");
			intArray[i] = inputScanner.nextInt();	
			intSum = intSum + intArray[i]; //calculates sum
			intProduct = intProduct * intArray[i]; //calculates product
		}
		System.out.println("Suma podanych przez Ciebie liczb wynosi: " + intSum);
		System.out.println("Iloczyn podanych przez Ciebie liczb wynosi: " + intProduct);
	}

	}
}
