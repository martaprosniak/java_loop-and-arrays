package pl.zadania.zadanie2; 

import java.util.Scanner;

/**
 *calculates n!
 *@author Joker
 */

public class Zadanie2 {

	public static void main(String[] args) {
	
	int intNumber; 
	int intSilnia = 1;

	Scanner inputScanner = new Scanner(System.in);
	System.out.println("Wprowadź liczbę, z której chcesz policzyć silnię:");
	intNumber = inputScanner.nextInt();
	//System.out.println(intNumber);

	if (intNumber < 0) {
		System.out.println("Nie można policzyć silni z liczby ujemnej!");
	} else if ((intNumber == 0) ^ (intNumber == 1)) {
		System.out.println("Silnia wynosi 1");
	} else {
		
		for (int i = 1; i <= intNumber; i++){
			intSilnia = intSilnia * i;	
		} 
		System.out.println("Silnia z " + intNumber + " wynosi " + intSilnia);
	}
	
		
	}
}

